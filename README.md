### 介绍
+ 计算机网络原理三级项目协作平台
+ 本次项目采用git进行团队协作开发
+ [下载git工具](https://git-scm.com/)
+ [跟着Git 教程敲一遍](https://www.runoob.com/git/git-tutorial.html)
+ 利用C#语言，基于.NET Frame
+ 项目名称智能工厂设备健康管理系统



### 网络协议
数据格式：以逗号隔开
+ 通信类别 1表示模拟端发往服务器
+ 设备id
+ 设备状态（运行、关闭）
+ 设备振动信息
+ 温度信息
+ 压力信息
+ 张力信息



### 参考
+ [C#_Socket实现聊天程序视频](https://www.bilibili.com/video/BV1eK4y1C7ri)
+ [c#导入项目后打开窗体设计界面](https://blog.csdn.net/qq_41146650/article/details/105545181)