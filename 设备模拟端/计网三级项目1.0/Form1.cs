﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 计网三级项目1._0
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
      
        private Socket socket;
        /*
         * 这里压力、温度、温度、张力是指系数
         * 这里假设压力、温度、温度、张力都是经过一定处理的
         * 比如：（设备的真实值/设备的参考值）*系数 + 偏置量
         * 并且假定 这个范围在[0,100]，且处于[40-60]是正常的
         * 
         * **/
        private String ID="1";
        private decimal shake = 50;
        private decimal temp = 50;
        private decimal pressure = 50;
        private decimal tension = 50;
        Random random = new Random();//设置随机数，使得参数在一段时间变化

        private void show()
        {   
            //对显示左侧的信息
            label1ID.Text = ID;
            if (buttonStart.Enabled==false)
            {
                label1Power.Text = "on";
                label1Power.BackColor = Color.Green;
            }
            else
            {
                label1Power.Text = "off";
                label1Power.BackColor = Color.Red;
            }
            //震动在这里规定一个系数40-60良，其他的差
            label1Shake.Text = shake.ToString()+"Shake";
            if (shake >= 40 && shake <= 60) label1Shake.BackColor = Color.Green;
            else label1Shake.BackColor = Color.Red;
            //这里规定温度40-60良，其他的差
            label1T.Text = temp.ToString()+"T";
            if (temp >= 40&&temp<=60) label1T.BackColor = Color.Green;
            else label1T.BackColor = Color.Red;
            //压力在这里规定一个系数40-60良，其他的差
            label1Pressure.Text = pressure.ToString()+"Pressure";
            if (pressure >= 40 && pressure <= 60) label1Pressure.BackColor = Color.Green;
            else label1Pressure.BackColor = Color.Red;
            //张力力在这里规定一个系数40-60良，其他的差
            label1Tension.Text = tension.ToString()+"Tension";
            if (tension >= 40&&tension<=60) label1Tension.BackColor = Color.Green;
            else label1Tension.BackColor = Color.Red;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            buttonEnd.Enabled = false;
            //对右侧的输入框进行初始化
            textBoxID.Text = ID;
            numericUpDownShake.Value = shake;
            numericUpDownT.Value = tension;
            numericUpDownPressure.Value = pressure;
            numericUpDownTension.Value = tension;
            show();
            timerUpdate.Enabled = true;
            timerUpdate.Start();
            timerUpdate.Interval = 3000;//设置每3秒自动更新
           //1、创建socket
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
	//2、连接服务器
                socket.Connect(new IPEndPoint(IPAddress.Parse("127.0.0.1"), 6666));
            }
            catch
            {
                MessageBox.Show("连接服务器失败");
                return;
            }
            //向服务器首次发送数据，发送自己的身份信息
            byte[] data = Encoding.Default.GetBytes("dev" + textBoxID.Text);
            socket.Send(data, 0, data.Length, SocketFlags.None);

        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            timerSendMsg.Enabled = true;
            timerSendMsg.Start();
            timerSendMsg.Interval = 10000;
            buttonStart.Enabled = false;
            buttonEnd.Enabled = true;
        }

        /*
          数据格式：以逗号隔开
            + 通信类别 1表示模拟端发往服务器
            + 设备id
            + 设备状态:（运行 1 、关闭 0）
            + 设备振动信息
            + 温度信息
            + 压力信息
            + 张力信息
         */

        private void timerSendMsg_Tick(object sender, EventArgs e)
        {
            try
            {
                if (socket.Connected)
                {
                    //先判断是否还有服务器处于连接状态
                    String msg = "1," + textBoxID.Text + ",";
                    if (buttonStart.Enabled==false) msg += "1,";
                    else msg += "0,";
                    msg += numericUpDownShake.Value + "," + numericUpDownT.Value + ",";
                    msg += numericUpDownPressure.Value + "," + numericUpDownTension.Value;
                    byte[] data = Encoding.Default.GetBytes(msg);
                    socket.Send(data, 0, data.Length, SocketFlags.None);
                }
                else
                {
                    MessageBox.Show("与服务器断开连接");
                    stopConnect();
                    timerSendMsg.Stop();
                }
            }
            catch
            {

            }
           
        }

        private void stopConnect()
        { 
            // 关闭连接
            try
            {
                if (socket!=null&&socket.Connected)
                {
                    socket.Shutdown(SocketShutdown.Both);
                    socket.Close(100);
                }
            }
            catch
            {

            }

        }

        private void buttonEnd_Click(object sender, EventArgs e)
        {
            buttonStart.Enabled = true;
            buttonEnd.Enabled = false;
            timerSendMsg.Stop();
            timerSendMsg.Enabled = false;
        }

        private void timerUpdate_Tick(object sender, EventArgs e)
        {
            if (buttonStart.Enabled==false)
            {
                if (random.Next(10) > 8)
                {
                    numericUpDownShake.Value += random.Next(-2, 3);
                    shake = numericUpDownShake.Value;
                    numericUpDownT.Value += random.Next(-2, 3);
                    temp = numericUpDownT.Value;
                    numericUpDownPressure.Value += random.Next(-2, 3);
                    pressure = numericUpDownPressure.Value;
                    numericUpDownTension.Value += random.Next(-2, 3);
                    tension = numericUpDownTension.Value;
                }
            }
            else
            {
                shake = pressure = tension =temp= 0;
            }
            show();
        }
    }
}
