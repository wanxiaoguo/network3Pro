﻿namespace 计网三级项目1._0
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label1Tension = new System.Windows.Forms.Label();
            this.label1Pressure = new System.Windows.Forms.Label();
            this.label1T = new System.Windows.Forms.Label();
            this.label1Shake = new System.Windows.Forms.Label();
            this.label1Power = new System.Windows.Forms.Label();
            this.label1ID = new System.Windows.Forms.Label();
            this.label0 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.textBoxID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.numericUpDownTension = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownPressure = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownT = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownShake = new System.Windows.Forms.NumericUpDown();
            this.labelTension = new System.Windows.Forms.Label();
            this.labelPressure = new System.Windows.Forms.Label();
            this.labelT = new System.Windows.Forms.Label();
            this.labelShake = new System.Windows.Forms.Label();
            this.buttonEnd = new System.Windows.Forms.Button();
            this.buttonStart = new System.Windows.Forms.Button();
            this.timerSendMsg = new System.Windows.Forms.Timer(this.components);
            this.timerUpdate = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTension)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPressure)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownShake)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.AllowDrop = true;
            this.splitContainer1.Panel1.Controls.Add(this.label1Tension);
            this.splitContainer1.Panel1.Controls.Add(this.label1Pressure);
            this.splitContainer1.Panel1.Controls.Add(this.label1T);
            this.splitContainer1.Panel1.Controls.Add(this.label1Shake);
            this.splitContainer1.Panel1.Controls.Add(this.label1Power);
            this.splitContainer1.Panel1.Controls.Add(this.label1ID);
            this.splitContainer1.Panel1.Controls.Add(this.label0);
            this.splitContainer1.Panel1.Controls.Add(this.pictureBox1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.textBoxID);
            this.splitContainer1.Panel2.Controls.Add(this.label1);
            this.splitContainer1.Panel2.Controls.Add(this.numericUpDownTension);
            this.splitContainer1.Panel2.Controls.Add(this.numericUpDownPressure);
            this.splitContainer1.Panel2.Controls.Add(this.numericUpDownT);
            this.splitContainer1.Panel2.Controls.Add(this.numericUpDownShake);
            this.splitContainer1.Panel2.Controls.Add(this.labelTension);
            this.splitContainer1.Panel2.Controls.Add(this.labelPressure);
            this.splitContainer1.Panel2.Controls.Add(this.labelT);
            this.splitContainer1.Panel2.Controls.Add(this.labelShake);
            this.splitContainer1.Panel2.Controls.Add(this.buttonEnd);
            this.splitContainer1.Panel2.Controls.Add(this.buttonStart);
            this.splitContainer1.Size = new System.Drawing.Size(1048, 539);
            this.splitContainer1.SplitterDistance = 718;
            this.splitContainer1.TabIndex = 0;
            // 
            // label1Tension
            // 
            this.label1Tension.AutoSize = true;
            this.label1Tension.Location = new System.Drawing.Point(470, 89);
            this.label1Tension.Name = "label1Tension";
            this.label1Tension.Size = new System.Drawing.Size(71, 15);
            this.label1Tension.TabIndex = 7;
            this.label1Tension.Text = "1Tension";
            // 
            // label1Pressure
            // 
            this.label1Pressure.AutoSize = true;
            this.label1Pressure.Location = new System.Drawing.Point(375, 89);
            this.label1Pressure.Name = "label1Pressure";
            this.label1Pressure.Size = new System.Drawing.Size(79, 15);
            this.label1Pressure.TabIndex = 6;
            this.label1Pressure.Text = "1Pressure";
            // 
            // label1T
            // 
            this.label1T.AutoSize = true;
            this.label1T.Location = new System.Drawing.Point(336, 89);
            this.label1T.Name = "label1T";
            this.label1T.Size = new System.Drawing.Size(23, 15);
            this.label1T.TabIndex = 5;
            this.label1T.Text = "1T";
            // 
            // label1Shake
            // 
            this.label1Shake.AutoSize = true;
            this.label1Shake.Location = new System.Drawing.Point(264, 89);
            this.label1Shake.Name = "label1Shake";
            this.label1Shake.Size = new System.Drawing.Size(55, 15);
            this.label1Shake.TabIndex = 4;
            this.label1Shake.Text = "1Shake";
            // 
            // label1Power
            // 
            this.label1Power.AutoSize = true;
            this.label1Power.Location = new System.Drawing.Point(203, 89);
            this.label1Power.Name = "label1Power";
            this.label1Power.Size = new System.Drawing.Size(55, 15);
            this.label1Power.TabIndex = 3;
            this.label1Power.Text = "1Power";
            // 
            // label1ID
            // 
            this.label1ID.AutoSize = true;
            this.label1ID.Location = new System.Drawing.Point(166, 89);
            this.label1ID.Name = "label1ID";
            this.label1ID.Size = new System.Drawing.Size(31, 15);
            this.label1ID.TabIndex = 2;
            this.label1ID.Text = "1ID";
            // 
            // label0
            // 
            this.label0.AutoSize = true;
            this.label0.Location = new System.Drawing.Point(203, 29);
            this.label0.Name = "label0";
            this.label0.Size = new System.Drawing.Size(213, 15);
            this.label0.TabIndex = 1;
            this.label0.Text = "ID/电源/震动/温度/压力/张力";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::计网三级项目1._0.Properties.Resources.Device;
            this.pictureBox1.Location = new System.Drawing.Point(12, 56);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(126, 92);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // textBoxID
            // 
            this.textBoxID.Location = new System.Drawing.Point(124, 60);
            this.textBoxID.Name = "textBoxID";
            this.textBoxID.Size = new System.Drawing.Size(100, 25);
            this.textBoxID.TabIndex = 14;
            this.textBoxID.Text = "1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 15);
            this.label1.TabIndex = 13;
            this.label1.Text = "ID";
            // 
            // numericUpDownTension
            // 
            this.numericUpDownTension.Location = new System.Drawing.Point(124, 193);
            this.numericUpDownTension.Name = "numericUpDownTension";
            this.numericUpDownTension.Size = new System.Drawing.Size(120, 25);
            this.numericUpDownTension.TabIndex = 10;
            // 
            // numericUpDownPressure
            // 
            this.numericUpDownPressure.Location = new System.Drawing.Point(124, 162);
            this.numericUpDownPressure.Name = "numericUpDownPressure";
            this.numericUpDownPressure.Size = new System.Drawing.Size(120, 25);
            this.numericUpDownPressure.TabIndex = 9;
            // 
            // numericUpDownT
            // 
            this.numericUpDownT.Location = new System.Drawing.Point(124, 131);
            this.numericUpDownT.Name = "numericUpDownT";
            this.numericUpDownT.Size = new System.Drawing.Size(120, 25);
            this.numericUpDownT.TabIndex = 8;
            // 
            // numericUpDownShake
            // 
            this.numericUpDownShake.Location = new System.Drawing.Point(124, 100);
            this.numericUpDownShake.Name = "numericUpDownShake";
            this.numericUpDownShake.Size = new System.Drawing.Size(120, 25);
            this.numericUpDownShake.TabIndex = 7;
            // 
            // labelTension
            // 
            this.labelTension.AutoSize = true;
            this.labelTension.Location = new System.Drawing.Point(11, 195);
            this.labelTension.Name = "labelTension";
            this.labelTension.Size = new System.Drawing.Size(75, 15);
            this.labelTension.TabIndex = 5;
            this.labelTension.Text = "张力（N）";
            // 
            // labelPressure
            // 
            this.labelPressure.AutoSize = true;
            this.labelPressure.Location = new System.Drawing.Point(11, 164);
            this.labelPressure.Name = "labelPressure";
            this.labelPressure.Size = new System.Drawing.Size(75, 15);
            this.labelPressure.TabIndex = 4;
            this.labelPressure.Text = "压力（N）";
            // 
            // labelT
            // 
            this.labelT.AutoSize = true;
            this.labelT.Location = new System.Drawing.Point(11, 133);
            this.labelT.Name = "labelT";
            this.labelT.Size = new System.Drawing.Size(82, 15);
            this.labelT.TabIndex = 3;
            this.labelT.Text = "温度（℃）";
            // 
            // labelShake
            // 
            this.labelShake.AutoSize = true;
            this.labelShake.Location = new System.Drawing.Point(11, 102);
            this.labelShake.Name = "labelShake";
            this.labelShake.Size = new System.Drawing.Size(37, 15);
            this.labelShake.TabIndex = 2;
            this.labelShake.Text = "震动";
            // 
            // buttonEnd
            // 
            this.buttonEnd.AutoSize = true;
            this.buttonEnd.Enabled = false;
            this.buttonEnd.Location = new System.Drawing.Point(124, 25);
            this.buttonEnd.Name = "buttonEnd";
            this.buttonEnd.Size = new System.Drawing.Size(75, 25);
            this.buttonEnd.TabIndex = 1;
            this.buttonEnd.Text = "结束";
            this.buttonEnd.UseVisualStyleBackColor = true;
            this.buttonEnd.Click += new System.EventHandler(this.buttonEnd_Click);
            // 
            // buttonStart
            // 
            this.buttonStart.AutoSize = true;
            this.buttonStart.Location = new System.Drawing.Point(14, 25);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(75, 25);
            this.buttonStart.TabIndex = 0;
            this.buttonStart.Text = "开始";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // timerSendMsg
            // 
            this.timerSendMsg.Tick += new System.EventHandler(this.timerSendMsg_Tick);
            // 
            // timerUpdate
            // 
            this.timerUpdate.Tick += new System.EventHandler(this.timerUpdate_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1048, 539);
            this.Controls.Add(this.splitContainer1);
            this.Name = "Form1";
            this.Text = "模拟设备端";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTension)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPressure)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownShake)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.NumericUpDown numericUpDownTension;
        private System.Windows.Forms.NumericUpDown numericUpDownPressure;
        private System.Windows.Forms.NumericUpDown numericUpDownT;
        private System.Windows.Forms.NumericUpDown numericUpDownShake;
        private System.Windows.Forms.Label labelTension;
        private System.Windows.Forms.Label labelPressure;
        private System.Windows.Forms.Label labelT;
        private System.Windows.Forms.Label labelShake;
        private System.Windows.Forms.Button buttonEnd;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label0;
        private System.Windows.Forms.Label label1ID;
        private System.Windows.Forms.Label label1Tension;
        private System.Windows.Forms.Label label1Pressure;
        private System.Windows.Forms.Label label1T;
        private System.Windows.Forms.Label label1Shake;
        private System.Windows.Forms.Label label1Power;
        private System.Windows.Forms.Timer timerSendMsg;
        private System.Windows.Forms.TextBox textBoxID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timerUpdate;
    }
}

