﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace chat_client
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public Socket clientSocket;
       

        private void buttonStart_Click(object sender, EventArgs e)
        {
            // 客户端连接服务器
            // 1 创建socket对象
            clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            //2、绑定ip和端口
            String ip = textBoxIP.Text;
            int port = Convert.ToInt32(textBoxPort.Text);
            try
            {
                clientSocket.Connect(new IPEndPoint(IPAddress.Parse(ip), port));
            }
            catch
            {
                MessageBox.Show("连接服务器失败");
            }
            //4、发送name
            byte[] data = Encoding.Default.GetBytes(textBoxName.Text);
            clientSocket.Send(data, 0, data.Length, SocketFlags.None);
            //3、接受消息
            Thread thread = new Thread(new ParameterizedThreadStart(recevie));
            thread.IsBackground = true;// 设置为后台线程
            thread.Start(clientSocket);
            buttonStart.Enabled = false;
        }

        private void recevie(object socket)
        {
            var proxSocket = socket as Socket;
            byte[] data = new byte[1024 * 1024];
            String msg;
            int len = 0;
            while (true)
            {
                try
                {
                    len = proxSocket.Receive(data, 0, data.Length, SocketFlags.None);
                }
                catch
                {
                    stopConnect();//关闭连接
                    return;
                }

                if (len <= 0)
                {
                    //客户端正常退出
                    msg = String.Format("服务器{0}发送异常",
                   proxSocket.RemoteEndPoint.ToString());
                    stopConnect();//关闭连接
                    return;//结束当前接受客户端数据的异步线程
                }
                //显示消息
                msg = Encoding.Default.GetString(data, 0, len);
                showLog(msg);
            }
        }

        private void stopConnect()
        {
            try
            {
                if (clientSocket.Connected)
                {
                    clientSocket.Shutdown(SocketShutdown.Both);
                    clientSocket.Close(100);
                }
            }
            catch
            {

            }
           
        }

        private void showLog(String msg)
        {
            if (textBoxLog.InvokeRequired)
            {
                //如果是跨线程访问
                textBoxLog.Invoke(new Action<String>(
                   s => {
                       this.textBoxLog.Text += msg + "\r\n";
                   }), msg);
            }
            else
            {
                this.textBoxLog.Text += msg;
            }
        }

        private void buttonSend_Click(object sender, EventArgs e)
        {
            if (clientSocket.Connected)
            {
                //先判断是否还有服务器处于连接状态
                byte[] data = Encoding.Default.GetBytes(textBoxMsg.Text);
                clientSocket.Send(data, 0, data.Length, SocketFlags.None);
                textBoxMsg.Text = "";
            }
            else
            {
                MessageBox.Show("与服务器断开连接");
            }
        }
    }
}
