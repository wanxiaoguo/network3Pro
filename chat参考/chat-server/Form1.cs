﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace chat_server
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

       
        private void Form1_Load(object sender, EventArgs e)
        {

        }
        // socket连接容器
        Dictionary<Socket, String> userContain = new Dictionary<Socket, string>();
        

        private void buttonStart_Click(object sender, EventArgs e)
        {
            try
            {
                //1、创建socket
                Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                //2、绑定ip和端口
                String ip = textBoxIP.Text;
                int port = Convert.ToInt32(textBoxPort.Text);
                socket.Bind(new IPEndPoint(IPAddress.Parse(ip), port));
                //3、开启监听
                socket.Listen(10);//等待连接队列的最大值
                //4、开始接受客户端的链接
                ThreadPool.QueueUserWorkItem(new WaitCallback(connect), socket);
            }
            catch
            {
                MessageBox.Show("启动服务器失败");
            }

        }
        
        private void connect(object socket)
        {
            var serverSockert = socket as Socket;//强制转换
            showLog("服务器正常启动，开始接受客户端的数据");
            byte[] data = new byte[1024];
            int len;
            String name; //客户端的用户名
            while (true)
            {
                try
                {
                    var proxSocket = serverSockert.Accept();//接受连接
                    len = proxSocket.Receive(data, 0, data.Length, SocketFlags.None);//接受客户端的用户名
                    name = Encoding.Default.GetString(data, 0, len);
                    showLog(String.Format("客户端 {0} 用户名 {1} 连接服务器", proxSocket.RemoteEndPoint.ToString(),name));
                    String msg = String.Format("用户{0}上线了", name);
                    sendMsg(msg);
                    userContain[proxSocket] = name;//把对象放入集合中
                    //不停的接受当前链接的客户端发送的消息
                    ThreadPool.QueueUserWorkItem(new WaitCallback(this.recevie), proxSocket);
                }
                catch
                {
                    MessageBox.Show("接受异常");
                    break;
                }
            }
        }

        private void recevie(object socket)
        {
            var proxSocket = socket as Socket;
            byte[] data = new byte[1024 * 1024];//接受，发送数据缓冲区
            String msg;
            int len = 0; // 数据长度
            String name = userContain[proxSocket]; // 客户端名字
            while (true)
            {
                try
                {
                    len = proxSocket.Receive(data, 0, data.Length, SocketFlags.None);
                }
                catch
                {
                    msg = String.Format("客户端{0}异常退出",
                    proxSocket.RemoteEndPoint.ToString());
                    showLog(msg);
                    msg = String.Format("用户{0}下线了", name);
                    sendMsg(msg);
                    userContain.Remove(proxSocket);
                    stopConnect(proxSocket);
                    return;
                }
          
                if (len <= 0)
                {
                    //客户端正常退出
                    msg = String.Format("客户端{0}正常退出",
                    proxSocket.RemoteEndPoint.ToString());
                    showLog(msg);
                    msg = String.Format("用户{0}下线了", name);
                    sendMsg(msg);
                    userContain.Remove(proxSocket);
                    stopConnect(proxSocket);
                    return;//结束当前接受客户端数据的异步线程
                }
                //接受消息
                msg = Encoding.Default.GetString(data, 0, len);
                //私聊信息格式@name:msg
                //name 为用户名 msg 为消息
                bool flag = true;
                if (msg.StartsWith("@"))
                {
                    int index = msg.IndexOf(":");
                    String targetName = msg.Substring(1, index-1);
                    msg = msg.Substring(index + 1);
                    foreach(var user in userContain)
                    {
                        if(targetName.Equals(user.Value)&&user.Key.Connected)
                        {
                            msg = String.Format("用户{0} 单独对你说：{1}",name,msg);
                            data = Encoding.Default.GetBytes(msg);
                            user.Key.Send(data, 0, data.Length, SocketFlags.None);
                            flag = false;
                            break;
                        }
                    }
                }
                if (flag)
                {
                    msg = String.Format("用户{0}：{1}", name, msg);
                    sendMsg(msg);
                }
            }
        }

        private void stopConnect(Socket socket)
        {
            try
            {
                if (socket.Connected)
                {
                    socket.Shutdown(SocketShutdown.Both);
                    socket.Close(100);
                }
            }
            catch
            {

            }
        }

        private void showLog(String msg)
        {
            if (textBoxLog.InvokeRequired)
            {
                //如果是跨线程访问
                textBoxLog.Invoke(new Action<String>(
                   s => {
                       this.textBoxLog.Text += msg+"\r\n"; 
                   }),msg);
            }
            else
            {
                this.textBoxLog.Text += msg;
            }
        }

        private void buttonSend_Click(object sender, EventArgs e)
        {
            //发送消息
            String msg = String.Format("服务器发布通知信息{0}", textBoxMsg.Text);
            sendMsg(msg);
        }

        private void sendMsg(String msg)
        {
            byte[] data = new byte[1024 * 1024];
            data = Encoding.Default.GetBytes(msg);
            foreach (var user in userContain)
            {
                if (user.Key.Connected)
                {
                     user.Key.Send(data, 0, data.Length, SocketFlags.None);
                }
            }
        }
    }
}
