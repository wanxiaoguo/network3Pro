﻿namespace 设备客户端
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxLog = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonSever = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonSS = new System.Windows.Forms.Button();
            this.buttonE1 = new System.Windows.Forms.Button();
            this.buttonS1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonS2 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonE2 = new System.Windows.Forms.Button();
            this.buttonS3 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.buttonE3 = new System.Windows.Forms.Button();
            this.buttonS5 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.buttonE5 = new System.Windows.Forms.Button();
            this.buttonS4 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.buttonE4 = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.开始ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.暂停ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.刷新ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.退出ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button13 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.button14 = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.button15 = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxLog
            // 
            this.textBoxLog.Location = new System.Drawing.Point(706, 60);
            this.textBoxLog.Multiline = true;
            this.textBoxLog.Name = "textBoxLog";
            this.textBoxLog.Size = new System.Drawing.Size(332, 407);
            this.textBoxLog.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(843, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "日志";
            // 
            // buttonSever
            // 
            this.buttonSever.Location = new System.Drawing.Point(72, 189);
            this.buttonSever.Name = "buttonSever";
            this.buttonSever.Size = new System.Drawing.Size(104, 88);
            this.buttonSever.TabIndex = 2;
            this.buttonSever.Text = "服务端";
            this.buttonSever.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(80, 293);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "状态：";
            // 
            // buttonSS
            // 
            this.buttonSS.BackColor = System.Drawing.Color.Lime;
            this.buttonSS.Location = new System.Drawing.Point(138, 289);
            this.buttonSS.Name = "buttonSS";
            this.buttonSS.Size = new System.Drawing.Size(22, 22);
            this.buttonSS.TabIndex = 4;
            this.buttonSS.UseVisualStyleBackColor = false;
            // 
            // buttonE1
            // 
            this.buttonE1.Location = new System.Drawing.Point(220, 92);
            this.buttonE1.Name = "buttonE1";
            this.buttonE1.Size = new System.Drawing.Size(84, 45);
            this.buttonE1.TabIndex = 5;
            this.buttonE1.Text = "设备1";
            this.buttonE1.UseVisualStyleBackColor = true;
            // 
            // buttonS1
            // 
            this.buttonS1.BackColor = System.Drawing.Color.Lime;
            this.buttonS1.Location = new System.Drawing.Point(277, 139);
            this.buttonS1.Name = "buttonS1";
            this.buttonS1.Size = new System.Drawing.Size(22, 22);
            this.buttonS1.TabIndex = 7;
            this.buttonS1.UseVisualStyleBackColor = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(224, 143);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "状态：";
            // 
            // buttonS2
            // 
            this.buttonS2.BackColor = System.Drawing.Color.Lime;
            this.buttonS2.Location = new System.Drawing.Point(456, 139);
            this.buttonS2.Name = "buttonS2";
            this.buttonS2.Size = new System.Drawing.Size(22, 22);
            this.buttonS2.TabIndex = 10;
            this.buttonS2.UseVisualStyleBackColor = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(403, 143);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 15);
            this.label4.TabIndex = 9;
            this.label4.Text = "状态：";
            // 
            // buttonE2
            // 
            this.buttonE2.Location = new System.Drawing.Point(399, 92);
            this.buttonE2.Name = "buttonE2";
            this.buttonE2.Size = new System.Drawing.Size(84, 45);
            this.buttonE2.TabIndex = 8;
            this.buttonE2.Text = "设备2";
            this.buttonE2.UseVisualStyleBackColor = true;
            // 
            // buttonS3
            // 
            this.buttonS3.BackColor = System.Drawing.Color.Lime;
            this.buttonS3.Location = new System.Drawing.Point(598, 207);
            this.buttonS3.Name = "buttonS3";
            this.buttonS3.Size = new System.Drawing.Size(22, 22);
            this.buttonS3.TabIndex = 13;
            this.buttonS3.UseVisualStyleBackColor = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(545, 211);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 15);
            this.label5.TabIndex = 12;
            this.label5.Text = "状态：";
            // 
            // buttonE3
            // 
            this.buttonE3.Location = new System.Drawing.Point(541, 160);
            this.buttonE3.Name = "buttonE3";
            this.buttonE3.Size = new System.Drawing.Size(84, 45);
            this.buttonE3.TabIndex = 11;
            this.buttonE3.Text = "设备3";
            this.buttonE3.UseVisualStyleBackColor = true;
            // 
            // buttonS5
            // 
            this.buttonS5.BackColor = System.Drawing.Color.Lime;
            this.buttonS5.Location = new System.Drawing.Point(319, 421);
            this.buttonS5.Name = "buttonS5";
            this.buttonS5.Size = new System.Drawing.Size(22, 22);
            this.buttonS5.TabIndex = 16;
            this.buttonS5.UseVisualStyleBackColor = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(266, 425);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 15);
            this.label6.TabIndex = 15;
            this.label6.Text = "状态：";
            // 
            // buttonE5
            // 
            this.buttonE5.Location = new System.Drawing.Point(262, 374);
            this.buttonE5.Name = "buttonE5";
            this.buttonE5.Size = new System.Drawing.Size(84, 45);
            this.buttonE5.TabIndex = 14;
            this.buttonE5.Text = "设备5";
            this.buttonE5.UseVisualStyleBackColor = true;
            // 
            // buttonS4
            // 
            this.buttonS4.BackColor = System.Drawing.Color.Lime;
            this.buttonS4.Location = new System.Drawing.Point(451, 310);
            this.buttonS4.Name = "buttonS4";
            this.buttonS4.Size = new System.Drawing.Size(22, 22);
            this.buttonS4.TabIndex = 19;
            this.buttonS4.UseVisualStyleBackColor = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(398, 314);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 15);
            this.label7.TabIndex = 18;
            this.label7.Text = "状态：";
            // 
            // buttonE4
            // 
            this.buttonE4.Location = new System.Drawing.Point(394, 263);
            this.buttonE4.Name = "buttonE4";
            this.buttonE4.Size = new System.Drawing.Size(84, 45);
            this.buttonE4.TabIndex = 17;
            this.buttonE4.Text = "设备4";
            this.buttonE4.UseVisualStyleBackColor = true;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.开始ToolStripMenuItem,
            this.暂停ToolStripMenuItem,
            this.刷新ToolStripMenuItem,
            this.退出ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1063, 28);
            this.menuStrip1.TabIndex = 20;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 开始ToolStripMenuItem
            // 
            this.开始ToolStripMenuItem.Name = "开始ToolStripMenuItem";
            this.开始ToolStripMenuItem.Size = new System.Drawing.Size(53, 24);
            this.开始ToolStripMenuItem.Text = "开始";
            this.开始ToolStripMenuItem.Click += new System.EventHandler(this.开始ToolStripMenuItem_Click);
            // 
            // 暂停ToolStripMenuItem
            // 
            this.暂停ToolStripMenuItem.Name = "暂停ToolStripMenuItem";
            this.暂停ToolStripMenuItem.Size = new System.Drawing.Size(53, 24);
            this.暂停ToolStripMenuItem.Text = "暂停";
            this.暂停ToolStripMenuItem.Click += new System.EventHandler(this.暂停ToolStripMenuItem_Click);
            // 
            // 刷新ToolStripMenuItem
            // 
            this.刷新ToolStripMenuItem.Name = "刷新ToolStripMenuItem";
            this.刷新ToolStripMenuItem.Size = new System.Drawing.Size(53, 24);
            this.刷新ToolStripMenuItem.Text = "刷新";
            // 
            // 退出ToolStripMenuItem
            // 
            this.退出ToolStripMenuItem.Name = "退出ToolStripMenuItem";
            this.退出ToolStripMenuItem.Size = new System.Drawing.Size(53, 24);
            this.退出ToolStripMenuItem.Text = "退出";
            this.退出ToolStripMenuItem.Click += new System.EventHandler(this.退出ToolStripMenuItem_Click);
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.Lime;
            this.button13.Location = new System.Drawing.Point(455, 383);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(22, 22);
            this.button13.TabIndex = 21;
            this.button13.UseVisualStyleBackColor = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(483, 387);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(142, 15);
            this.label8.TabIndex = 24;
            this.label8.Text = "：绿色表示正在工作";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(483, 415);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(217, 15);
            this.label9.TabIndex = 26;
            this.label9.Text = "：黄色表示设备未连接或未运行";
            // 
            // button14
            // 
            this.button14.BackColor = System.Drawing.Color.Orange;
            this.button14.Location = new System.Drawing.Point(455, 411);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(22, 22);
            this.button14.TabIndex = 25;
            this.button14.UseVisualStyleBackColor = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(483, 443);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(142, 15);
            this.label10.TabIndex = 28;
            this.label10.Text = "：红色表示出现故障";
            // 
            // button15
            // 
            this.button15.BackColor = System.Drawing.Color.Red;
            this.button15.Location = new System.Drawing.Point(455, 439);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(22, 22);
            this.button15.TabIndex = 27;
            this.button15.UseVisualStyleBackColor = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1063, 499);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.button15);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.button14);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.buttonS4);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.buttonE4);
            this.Controls.Add(this.buttonS5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.buttonE5);
            this.Controls.Add(this.buttonS3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.buttonE3);
            this.Controls.Add(this.buttonS2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.buttonE2);
            this.Controls.Add(this.buttonS1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.buttonE1);
            this.Controls.Add(this.buttonSS);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.buttonSever);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxLog);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "模拟客户端";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxLog;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonSever;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonSS;
        private System.Windows.Forms.Button buttonE1;
        private System.Windows.Forms.Button buttonS1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonS2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonE2;
        private System.Windows.Forms.Button buttonS3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button buttonE3;
        private System.Windows.Forms.Button buttonS5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button buttonE5;
        private System.Windows.Forms.Button buttonS4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button buttonE4;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 开始ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 暂停ToolStripMenuItem;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.ToolStripMenuItem 刷新ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 退出ToolStripMenuItem;
    }
}

