﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Net.Sockets;
using System.Net;


namespace 设备客户端
{
    public partial class Form1 : Form
    {
        Button[] B = new Button[9];
        Thread station= null;//定义线程
        int flag = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private Socket socket;
        int [] arr = new int[6];

        private void Form1_Load(object sender, EventArgs e)
        {
            B[0] = buttonSS;//服务器状态按钮
            B[1] = buttonS1;//设备1
            B[2] = buttonS2;
            B[3] = buttonS3;
            B[4] = buttonS4;
            B[5] = buttonS5;//设备5
            //B[5].BackColor = Color.Red;
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                socket.Connect(new IPEndPoint(IPAddress.Parse("127.0.0.1"), 6666));
                showLog("成功连接服务器");
            }
            catch
            {
                MessageBox.Show("连接服务器失败");
                return;
            }
            //向服务器首次发送数据，发送自己的身份信息
            byte[] data = Encoding.Default.GetBytes("client");
            socket.Send(data, 0, data.Length, SocketFlags.None);
            Thread thread = new Thread(new ParameterizedThreadStart(recevie));
            thread.IsBackground = true;//设置为后台线程
            thread.Start(socket);
            for (int i = 1; i < 6; ++i) arr[i] = 2;
            turnreal();//进行初始化
            station = new Thread(new ThreadStart(ss)) { IsBackground = true };
            station.Start();
        }

        private void recevie(object socket)
        {
            var proxSocket = socket as Socket;
            byte[] data = new byte[1024 * 1024];
            String msg;
            int len = 0;
            while (true)
            {
                try
                {
                    len = proxSocket.Receive(data, 0, data.Length, SocketFlags.None);
                }
                catch
                {
                   
                    return;
                }
                if (len <0)
                {
                    //客户端正常退出
                    return;//结束当前接受客户端数据的异步线程
                }
                //接受来自服务器消息
                /*
                   * 发送数据的格式，(以','隔开)
                   * 通信类别：2 （表示服务器发我客户端的数据）
                   * 设备id 
                   * 状态（正常运行 1，未运行 2，发送故障 3）
                   * 原因：如果发送故障，发送故障信息
               */
                msg = Encoding.Default.GetString(data, 0, len);

                String[] msgs = msg.Split(new char[] { ',' });
                //将服务器状态传给数组arr
                int a, b;
                a = Convert.ToInt32(msgs[1]);
                b = Convert.ToInt32(msgs[2]);
                arr[a] = b;
                turnreal();
                if (b == 3)
                {
                    showLog(msgs[3]);
                }
               
            }
        }

        private void turnreal()
        {
            for (int i = 0; i < 6; i++)
            {
                Turncolor(i,arr[i]);
            }
        }

        void Turncolor(int num,int state)
        {
            if(state==1)
            {
                B[num].BackColor = Color.Green;
            }
            else if(state==2)
            {
                B[num].BackColor = Color.Yellow;
            }
            else if(state==3)
            {
                B[num].BackColor = Color.Red;
            }
        }//改变状态

        void Turnshow()
        {
            for (int i = 0; i < 6; i++)
            {
                B[i].Show();
            }
        }//改变状态

        private void ss()
        {
            while(true)
            {
                System.Threading.Thread.Sleep(200);
                flag = 1 - flag;
                Action<int> actionShow = (x) => { B[x].Show(); flag = 0; };
                Action<int> actionHide = (x) => { B[x].Hide(); flag = 1; };
                for (int i = 0; i < 6; i++)
                {
                    if (B[i].InvokeRequired)
                    {
                        if (flag == 0)
                        {
                            B[i].Invoke(actionHide, i);

                        }
                        else
                        {
                            B[i].Invoke(actionShow, i);
                        }
                    }
                    else
                    {
                        if (flag == 0) { B[i].Hide(); flag = 1; }
                        else { B[i].Show(); flag = 0; }
                    }
                }
                
            }
        }//闪烁线程

        private void showLog(String msg)
        {
            if (textBoxLog.InvokeRequired)
            {
                //如果是跨线程访问
                textBoxLog.Invoke(new Action<String>(
                   s => {
                       this.textBoxLog.Text += msg + "\r\n";
                   }), msg);
            }
            else
            {
                this.textBoxLog.Text += msg;
            }
        }//输出日志

        private void 开始ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            station.Start();

        }//开始

        private void 暂停ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(暂停ToolStripMenuItem.Text == "继续")
            {
                station.Resume();
                暂停ToolStripMenuItem.Text = "暂停";
                Turnshow();
            }
            else
            {
                station.Suspend();
                暂停ToolStripMenuItem.Text = "继续";
                Turnshow();
            }
        }//暂停

        private void 退出ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
