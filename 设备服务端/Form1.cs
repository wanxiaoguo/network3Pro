﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace device
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        // 设备socket连接对象容器
        Dictionary<Socket, String> deviceContain = new Dictionary<Socket, string>();
      // 客户端socket连接对象
        Socket clientSocket=null;
        private void buttonStart_Click(object sender, EventArgs e)
        {
            try
            {
                //1、创建socket
                Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                //2、绑定ip和端口
                String ip = textBoxIP.Text;
                int port = Convert.ToInt32(textBoxPort.Text);
                socket.Bind(new IPEndPoint(IPAddress.Parse(ip), port));
                //3、开启监听
                socket.Listen(10);//等待连接队列的最大值
                //4、开始接受客户端的链接
                ThreadPool.QueueUserWorkItem(new WaitCallback(connect), socket);
            }
            catch
            {
                MessageBox.Show("启动服务器失败");
            }
        }

        private void connect(object socket)
        {
            var serverSockert = socket as Socket;//强制转换
            showLog("服务器正常启动，开始接受客户端的数据");
            byte[] data = new byte[1024];
            int len;
            String name; //客户端的用户名
            while (true)
            {
                try
                {
                    var proxSocket = serverSockert.Accept();//接受连接
                    //不停的接受当前链接的客户端发送的消息
                    len = proxSocket.Receive(data, 0, data.Length, SocketFlags.None);
                    name = Encoding.Default.GetString(data, 0, len);
                    if (name.StartsWith("dev"))
                    {
                        name = name.Substring(3);
                        showLog(String.Format("设备{0}开始连接服务器", name));
                        deviceContain[proxSocket] = name;
                    }
                    else
                    {
                        showLog("客户端连接服务器");
                        clientSocket = proxSocket;
                    }
                    ThreadPool.QueueUserWorkItem(new WaitCallback(this.recevie), proxSocket);
                }
                catch
                {
                    MessageBox.Show("接受异常");
                    break;
                }
            }
        }

        private void showLog(String msg)
        {
            if (textBoxLog.InvokeRequired)
            {
                //如果是跨线程访问
                textBoxLog.Invoke(new Action<String>(
                   s => {
                       this.textBoxLog.Text += msg + "\r\n";
                   }), msg);
            }
            else
            {
                this.textBoxLog.Text += msg;
            }
        }

        private void recevie(object socket)
        {
            var proxSocket = socket as Socket;
            byte[] data = new byte[1024 * 1024];//接受，发送数据缓冲区
            String msg;
            int len = 0; // 数据长度
            while (true)
            {
                try
                {
                    len = proxSocket.Receive(data, 0, data.Length, SocketFlags.None);
                }
                catch
                {
                    msg = String.Format("{0}异常退出",
                    proxSocket.RemoteEndPoint.ToString());
                    showLog(msg);
                    if (deviceContain.ContainsKey(proxSocket))
                    {
                        deviceContain.Remove(proxSocket);
                    } 
                    else if (clientSocket == proxSocket)
                    {
                        clientSocket = null;
                    }
                    stopConnect(proxSocket);
                    return;
                }

                if (len <0)
                {
                    //客户端正常退出
                    msg = String.Format("{0}异常退出",
                    proxSocket.RemoteEndPoint.ToString());
                    showLog(msg);
                    if (deviceContain.ContainsKey(proxSocket))
                    {
                        deviceContain.Remove(proxSocket);
                    }
                    else if (clientSocket == proxSocket)
                    {
                        clientSocket = null;
                    }
                    stopConnect(proxSocket);
                    return;//结束当前接受客户端数据的异步线程
                }
                //接受消息
                msg = Encoding.Default.GetString(data, 0, len);
                //对数据进行处理
                String [] msgs=msg.Split(new char[] { ','});
                /*
                 * 发送数据的格式，(以','隔开)
                 * 通信类别：2 （表示服务器发我客户端的数据）
                 * 设备id 
                 * 状态（正常运行 1，未运行 2，发送故障 3）
                 * 原因：如果发送故障，发送故障信息
                 */
                String sendMsg = "2,";
                sendMsg += msgs[1]+",";//设备id
                String fault = "";//故障信息
                if (msgs[2] == "1")
                {
                    //判断是否故障
                    if (Convert.ToDecimal(msgs[3])<40 ) {
                        fault += "设备振动系数低于正常范围;";
                    }else if(Convert.ToDecimal(msgs[3]) > 60)
                    {
                        fault += "设备振动系数高于正常范围;";
                    }
                    if (Convert.ToDecimal(msgs[4]) < 40)
                    {
                        fault += "设备温度系数低于正常范围;";
                    }
                    else if (Convert.ToDecimal(msgs[4]) > 60)
                    {
                        fault += "设备温度系数高于正常范围;";
                    }
                    if (Convert.ToDecimal(msgs[5]) < 40)
                    {
                        fault += "设备压力系数低于正常范围;";
                    }
                    else if (Convert.ToDecimal(msgs[5]) > 60)
                    {
                        fault += "设备压力系数高于正常范围;";
                    }
                    if (Convert.ToDecimal(msgs[6]) < 40)
                    {
                        fault += "设备张力系数低于正常范围;";
                    }
                    else if (Convert.ToDecimal(msgs[6]) > 60)
                    {
                        fault += "设备张力系数高于正常范围;";
                    }
                    if (fault.Length == 0)
                    {
                        sendMsg += "1";
                        fault = "正常运行";
                    }
                    else
                    {
                        sendMsg += "3," + fault;
                    }
                }
                else
                {
                    sendMsg += "2";
                }
                if (clientSocket != null&&clientSocket.Connected)
                {
                    data = Encoding.Default.GetBytes(sendMsg);
                    clientSocket.Send(data, 0, data.Length, SocketFlags.None);
                }
                showLog(String.Format("设备{0}: {1}",msgs[1],fault));
            }
        }

        private void stopConnect(Socket socket)
        {
            try
            {
                if (socket.Connected)
                {
                    socket.Shutdown(SocketShutdown.Both);
                    socket.Close(100);
                }
            }
            catch
            {

            }
        }
    }
}
/*
  数据格式：以逗号隔开
    + 通信类别
    + 设备id
    + 设备状态（运行、关闭）
    + 设备振动信息
    + 温度信息
    + 压力信息
    + 张力信息
 */
